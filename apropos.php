<?php include 'inc/header.php'; ?>

<section class="section">
  <div class="container">
    <h2 class="title is-2">À propos</h2>

    <h2 class="title is-3">Travail en Cours.</h2>

    <div class="content">
      <a href="https://pads.erg.be/p/POST-ERG_charteDeVosMEMOIRES">Chartes d'utilisation et fonctionnement de l'initiative.</a>
    </div>

    <div class="content">
      <p>Nous sommes un groupe d'étudiant.e.s en design numérique avec projet concernant les mémoires de l'année
        passée qui demande votre aide !

        Qu'en est-il des mémoires après notre master ?
        Quelle est la visibilité de notre travail après notre départ de l'ERG ? Certains mémoires finissent à la
        bibliothèque exposés, mais lesquels et pourquoi ?
        Actuellement, la bibliothèque (BAUI) sert de lieux d'archives (collection de documents anciens, classés à des
        fins historiques ; lieu où les archives sont conservées) des mémoires pour l'erg, st-Luc et UCL mais pourquoi
        sont-ils si peu à être exposés ? </p>

      <p>Actuellement, les mémoires sélectionnés sont ceux avec une grande disctinction (16/20). Cette note obtenue
        dépend de la cotation de lecture de mémoire et sa défense.</p>

      <h3 class="title is-3">Mais pourquoi cette moyenne de 14/20 ?</h3>
      <h3 class="title is-3">Et où finissent les autres mémoires ?</h3>

      <p>En l'occurence, la bibliothèque n'est pas un lieu de diffusion et de monstration " juste ", car les mémoires
        dépendent de la note attribuée en fin de Master et de la place disponible dans les étagères ; sans parler de
        l'état déplorable de certains mémoires due aux conditions de stockages : couverture plastifiée, stickers, etc
        - nous travaillons un visuel qui finalement sera " dégradé " lors de son exposition à la bibliothèque, si
        exposé. De plus, les mémoires sont visible en bibliothèque de manière tangible (style édition).</p>

      <h3 class="title is-3">Qu'en t-il des formats numérique, audio ou vidéo ?</h3>

      <p>De fait, notre recherche se pencherait sur un dispositif de partage/diffusion plus adéquat et en phase avec
        la multitude de format et forme de monstration plus contemporain.
        <br>
        Notre lieu d'archive/exposition prendrait la forme d'un site web, idéalement en ligne (ou en local en fonction
        du RE - propriété intellectuelle et droit d'auteur ?). Il contiendrait tout types de mémoire ainsi qu'une
        interrogation autour de sa licence et sa notion de partage.
        <br>
        En paralèlle, nous donnerons quelques tips et bon plans pour : " comment licencier son mémoire : pour protéger
        ses valeurs et notions de partage s'il y'a" . Dans un premier temps, nous allons collecter un maximum de
        mémoires et
        tenter de recontacter leur auteurice pour échanger avec eux et obtenir des pdf, vidéos, photos. Dans un second
        temps, nous trouverons un relais pour les futures étudiant.e.s et organiseront un formulaire qui publiera
        automatiquement les mémoires sur notre site.
      </p>

      <h2 class="title is-2">Un projet depuis 2022</h2>

      <p>
        Théo Hennequin<br>
        <a href="https://www.theohennequin.com">www.theohennequin.com</a><br>
        Théophile Gervreau-Mercier<br>
        <a href="https://tgm.happyngreen.fr">tgm.happyngreen.fr</a><br>
        Olivia Marly<br>
        <a href="mailto:oli98marly@gmail.com">oli98marly@gmail.com</a>
      </p>
    </div>
  </div>
</section>

      <?php include 'inc/footer.php'; ?>
<?php include 'inc/header.php'; ?>
<section class="section">
  <div class="container">
    <h2 class="title is-2">Contact</h2>
    <div class="content">

      <p>Laurent Leprince, <br>
        Bibliothèque d'architecture, d'ingénierie architecturale, d'urbanisme (BAIU) :<br>
        laurent.leprince@uclouvain.be</p>

      <p>Xavier Gorgol, <br>
        Responsable des mémoires de l'ERG :<br>
        xavier.gorgol@erg.be<br></p>

      <p>Brigitte Ledune,<br>
        Cours de suivi de mémoire : <br>
        brigitte.ledune@erg.be</p>

      <?php include 'inc/footer.php'; ?>
#!/bin/bash

# Download the index page and convert local links to global links
url="https://www.memo-dg.fr/"
curl $url > index.html

# Extract the image URLs from the downloaded HTML file
cat index.html | grep img | grep -Po 'src="\K.*?(?=")' | sed 's/\?.*//' > links.txt

# Download all the images using curl
while read -r file; do
    curl -s -O $file
done < links.txt

# Clean up
rm index.html links.txt

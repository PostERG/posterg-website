<!-- header.php -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Posterg</title>
    <link rel="stylesheet" href="assets/normalize.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
    <link rel="stylesheet" href="assets/posterg.css">
</head>

<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="index.php">
                <h1 class="title is-1">Mémoire post-ERG/A life after ERG</h1>
            </a>
        </div>
        <div class="navbar-menu">
            <div class="navbar-end">
                <!-- <input type="search" name="search" placeholder="Recherche..." button class="boutton boutton1"></input> -->
                <!-- <a href="partagerunmemoire.html" button class="boutton boutton1">Partager un mémoire</a> -->
                <a href="apropos.php" class="navbar-item">À propos</a>
                <a href="contact.php" class="navbar-item">Contact</a>
                <a href="licences.php" class="navbar-item">Licences</a>
            </div>
        </div>
    </nav>
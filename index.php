<?php
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', 'error.log');

require_once 'vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$itemsPerPage = 10;

$dir = "data/yaml/*.yaml";
$yamlFiles = glob($dir);
$data = [];

foreach ($yamlFiles as $yamlFile) {
  $data[] = Yaml::parseFile($yamlFile);
}

usort($data, function ($a, $b) {
  return $a['année'] <=> $b['année'];
});

$offset = ($page - 1) * $itemsPerPage;
$itemsToLoad = array_slice($data, $offset, $itemsPerPage);

include 'inc/header.php'; ?>

<section class="section">
  <div class="container">
    <div class="columns is-multiline">

      <?php foreach ($itemsToLoad as $key => $item): ?>
        <div class="column is-one-fifth">
          <a href="memoire.php?file=<?= urlencode($yamlFiles[$key]); ?>" class="card-link">
            <div class="card">
              <?php if (isset($item['couverture'])): ?>
                <div class="card-image">
                  <figure class="image ">
                    <img src="<?= $item['couverture']; ?>" alt="Image preview">
                  </figure>
                </div>
              <?php endif; ?>
              <div class="card-content">
                <h4 class="title is-4">
                  <?= $item['titre']; ?>
                </h4>
                <h2 class="subtitle">
                  <?= $item['auteurice']; ?>
                </p>
                <h3 class="tag title is-6 is-link is-light">
                  <?= $item['année']; ?>
              </h3>
                <p class="block content">
                  <?php
                  $excerpt_length = 150;
                  $description_excerpt = substr($item['description'], 0, $excerpt_length) . '...';
                  ?>
                  <?= $description_excerpt; ?>
                </p>
              </div>

            </div>
          </a>
        </div>
      <?php endforeach; ?>

    </div>
  </div>
</section>

<?php
include 'inc/footer.php';
?>
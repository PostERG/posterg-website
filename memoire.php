<?php
// Disable displaying errors, log errors to a file named 'error.log'
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', 'error.log');

// Load required libraries and classes
require_once 'vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;

// Check if a file parameter is provided in the URL
if (isset($_GET['file'])) {
  // Decode the URL parameter and parse the YAML file
  $yamlFile = urldecode($_GET['file']);
  $data = Yaml::parseFile($yamlFile);
} else {
  // Redirect to the index page if no file parameter is provided
  header('Location: index.php');
  exit;
}

// Include the header template
include 'inc/header.php'; ?>

<section class="section">
  <div class="container">
    <div class="columns is-variable is-1-mobile is-0-tablet is-3-desktop is-8-widescreen is-2-fullhd">
      <!-- INFO CARD -->
      <div class="column is-one-third">
        <div class="card">
          <div class="card-content">
            <!-- Display the title and author from the YAML data -->
            <h1 class="title">
              <?= $data['titre']; ?>
            </h1>
            <h2 class="subtitle">par
              <?= $data['auteurice']; ?>
            </h2>

            <h3 class="subtitle"></h3>
            <div class="columns">
              <div class="column is-half ">
                <h3 class="subtitle">
                  <?= $data['orientation']; ?> et
                  <?= $data['ap']; ?>
                </h3>
                <p class="block tag subtitle is-6">
                  <?= $data['année']; ?>
                </p>
              </div>

              <div class="column">
                <p class="block">
                  <?= $data['problématique']; ?>
                </p>

                <p class="block">
                  <strong>Contact:</strong>
                  <?= $data['email']; ?>
                </p>
                <p class="block">
                  <strong>Promoteur.ice.s:</strong>
                  <?= $data['promoteurice']; ?>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <?= $data['description']; ?>
        </div>
      </div>

      <div class="column is-two-third">
        <div class="content">
          <!-- Check if there are any files in the YAML data -->
          <?php if (isset($data['files'])): ?>
            <!-- Loop through the files and display them based on their file type -->
            <?php foreach ($data['files'] as $file): ?>
              <?php $ext = pathinfo($file, PATHINFO_EXTENSION); ?>
              <div class="block">
                <?php if ($ext === 'pdf'): ?>
                  <!-- Display PDF files using the embed element -->
                  <embed src="<?= $file; ?>" type="application/pdf" width="100%" height="600px" />
                <?php elseif (in_array($ext, ['jpg', 'jpeg', 'png', 'gif', 'bmp'])): ?>
                  <!-- Display image files using the img element -->
                  <figure>
                    <img src="<?= $file; ?>" alt="Image file">
                  </figure>
                <?php elseif ($ext === 'mp4'): ?>
                  <!-- Display MP4 video files using the video element -->
                  <video width="100%" height="auto" controls>
                    <source src="<?= $file; ?>" type="video/mp4">
                    Your browser does not support the video tag.
                  </video>
                <?php endif; ?>
              </div>

            <?php endforeach; ?>
          <?php endif; ?>
        </div>

      </div>
    </div>
  </div>
</section>

<!-- Include the footer template -->
<?php include 'inc/footer.php'; ?>